const express = require("express")

const app = express()

const port = 4000

app.use(express.json())

let users = [
	{
		"username": "user1",
		"pass": "pass1",
		"name": "one",
		"isAdmin": false
	},
	{
		"username": "user2",
		"pass": "pass2",
		"name": "two",
		"isAdmin": false
	},
	{
		"username": "user3",
		"pass": "pass3",
		"name": "three",
		"isAdmin": true
	}
]

let loggedUser = {}

app.get('/',(req,res)=>{
	res.send("Hello World!")
})

app.get('/hello',(req,res)=>{
	res.send("Hello from Batch 123!")
})

app.post('/',(req,res)=>{
	console.log(req.body)
	res.send(`Hello, I'm ${req.body.name}`)
})

app.post('/users',(req, res)=>{
	console.log(req.body)

	let newUser = {
		name: req.body.name,
		username: req.body.username
	}

	users.push(newUser)
	console.log(users)

	res.send("Registered Successfully.")
})

app.post('/users/login', (req, res)=>{
	console.log(req.body)
	let foundUser = users.find((user)=>{
		return user.username === req.body.username && user.password === req.body.password
	})

	if(foundUser !== undefined){
		loggedUser = foundUser
		console.log(loggedUser)
		res.send("Login Succesful")
	}
	else{
		res.send("Loin Unseccesful")
	}
})

let itemsArray = [
	{
	"name": "item1",
	"desc": "desc1",
	"price": 200,
	"isActive": true
	},
	{
	"name": "item2",
	"desc": "desc2",
	"price": 700,
	"isActive": true
	},
	{
	"name": "item3",
	"desc": "desc3",
	"price": 150,
	"isActive": true
	}
]

app.post('/items', (req, res)=>{
	console.log(loggedUser)
	console.log(req.body)
	if(loggedUser.isAdmin === true){
		let newItem = {
			name: req.body.name,
			desc: req.body.desc,
			price: req.body.price,
			isActive: req.body.isActive
		}
		itemsArray.push(newItem)
		console.log(itemsArray)
		res.send("You have added a new item")
	}
	else{
		res.send("Unauthorized: Action Forbidden")
	}
})

app.post('/items/view', (req,res)=>{
	if(loggedUser.isAdmin === true){
		res.send(itemsArray)
	}
	else{
		res.send("Unauthorized: Action Forbidden")
	}
})

app.post('/users/admin/:index', (req,res)=>{
	let userIndex = parseInt(req.params.index);
	if(loggedUser.isAdmin === true){
		users[userIndex].isAdmin = true;
		res.send("User Authenticated As Admin.")
	}
	else{
		res.send("Unauthorized: Action Forbidden")
	}
})

app.post('/items/editPrice/:index', (req,res)=>{
	let itemIndex = parseInt(req.params.index);
	if(loggedUser.isAdmin === true){
		itemsArray[itemIndex].price = {
			newPrice: req.body.price
		}
		res.send('Item Price Edited.')
	} 
	else{
		res.send('Unauthorized: Action Forbidden.')
	}
})

app.listen(port, ()=>console.log(`Server is running at port  ${port}`))